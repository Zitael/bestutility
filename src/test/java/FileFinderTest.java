import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class FileFinderTest {
    private FileFinder subj = new FileFinder();

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Before
    public void setUp() throws Exception {
        folder.newFolder("temp");
        folder.newFile("test1.txt");
        folder.newFile("temp/test2.txt");
        folder.newFile("png.png");
    }

    @Test
    public void findFiles_WithInnerFolders() {
        List<File> list = subj.findFiles(folder.getRoot().getAbsolutePath(), "test");
        assertNotNull(list);
        assertEquals(list.size(), 2);
        assertEquals("test1.txt", list.get(1).getName());
        assertEquals("test2.txt", list.get(0).getName());
    }

    @Test
    public void findFiles_WhenNotTXTFiles() {
        List<File> list = subj.findFiles(folder.getRoot().getAbsolutePath(), "png");
        assertNotNull(list);
        assertEquals(list.size(), 0);
    }

    @Test
    public void modifyFiles_whenWritable() throws CustomException, IOException {
        List<File> list = Collections.singletonList(folder.newFile("txt.txt"));
        List<File> result = subj.modifyFiles(list);
        assertEquals(list.size(), result.size());
        File file = list.get(0);
        assertNotEquals(file.length(), 0);
        assertEquals(file.getName(), "txt.txt");
        assertEquals(result.get(0).getName(), "txt.txt");
    }

    @Test
    public void modifyFiles_whenNotWritable() throws CustomException, IOException {
        File testFile = folder.newFile("txt.txt");
        testFile.setWritable(false);
        List<File> list = Collections.singletonList(testFile);
        List<File> result = subj.modifyFiles(list);
        assertEquals(list.size(), result.size());
        File file = result.get(0);
        assertNotEquals(file.length(), 0);
        assertEquals(file.getName(), "tmptxt.txt");
    }
}