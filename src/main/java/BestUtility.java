import java.io.*;
import java.util.Arrays;
import java.util.List;

public class BestUtility {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Need command");
            showHelp();
            return;
        }
        String command = args[0];
        if ("-readDrives".equals(command)) {
            printFileList(Arrays.asList(File.listRoots()));
            return;
        } else if (args.length < 3) {
            System.out.println("Need path and filename");
            showHelp();
            return;
        }
        FileFinder finder = new FileFinder();
        String pathToFile = args[1];
        String fileName = args[2];
        if ("-findFiles".equals(command)) {
            printFileList(finder.findFiles(pathToFile, fileName));
        } else if ("-modifyFile".equals(command)) {
            List<File> files = finder.findFiles(pathToFile, fileName);
            printFileList(files);
            try {
                if (finder.modifyFiles(files).size() == files.size()) {
                    System.out.println("Success modified");
                } else {
                    System.out.println("Cant to modify all files");
                }
            } catch (CustomException e) {
                System.out.println("Error modify files, " + e.getMessage());
            }
        } else {
            System.out.println("Command not valid");
            showHelp();
        }
    }

    private static void printFileList(List<File> list) {
        if (list != null) {
            for (File entry : list) {
                System.out.println(entry);
            }
        }
    }

    private static void showHelp() {
        System.out.println("Available commands:");
        System.out.println("-readDrives - show local drives on device");
        System.out.println("-findFiles <path> <filename> - find all files at your path with custom name");
        System.out.println("-modifyFile <path> <filename> - added some string at start of founded by path and name files");
    }
}
