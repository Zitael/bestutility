import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileFinder {
    private final static String SOME_FIRST_STRING = "Some first string\n";

    public List<File> findFiles(String path, String fileName) {
        ArrayList<File> foundedFiles = new ArrayList<>();
        processFilesFromFolder(new File(path), fileName, foundedFiles);
        return foundedFiles;
    }

    private void processFilesFromFolder(File folder, String fileName, ArrayList<File> foundedFiles) {
        File[] folderEntries = folder.listFiles();
        if (folderEntries != null) {
            for (File entry : folderEntries) {
                if (entry.isDirectory()) {
                    processFilesFromFolder(entry, fileName, foundedFiles);
                } else if (isTextFile(entry) && entry.getName().contains(fileName)) {
                    foundedFiles.add(entry);
                }
            }
        }
    }

    private boolean isTextFile(File entry) {
        String extension = "";
        int i = entry.getName().lastIndexOf('.');
        if (i > 0) {
            extension = entry.getName().substring(i + 1);
        }
        return ("txt").equals(extension);
    }

    public List<File> modifyFiles(List<File> files) throws CustomException {
        List<File> modifiedList = new ArrayList<>();
        for (File entry : files) {
            try {
                File copy = copyFile(entry);
                if (entry.canWrite()) {
                    if (entry.delete() && copy.renameTo(entry)) {
                        modifiedList.add(entry);
                    } else {
                        throw new CustomException(entry.getAbsolutePath());
                    }
                } else {
                    modifiedList.add(copy);
                }
            } catch (Exception e) {
                throw new CustomException("Error modify file, " + e.getMessage());
            }
        }
        return modifiedList;
    }

    protected File copyFile(File entry) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(entry));
        File newFile = new File(entry.getParent() + "\\tmp" + entry.getName());
        BufferedWriter bw = new BufferedWriter(new FileWriter(newFile));
        bw.write(SOME_FIRST_STRING);
        while (br.ready()) {
            bw.write(br.readLine());
        }
        br.close();
        bw.close();
        return newFile;
    }
}
